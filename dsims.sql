-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: dsims
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cart_items`
--

DROP TABLE IF EXISTS `cart_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cart_items` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `total_price` double NOT NULL,
  `quantity` int NOT NULL,
  `cart_id` bigint NOT NULL,
  `inventory_item_id` bigint DEFAULT NULL,
  `product_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpcttvuq4mxppo8sxggjtn5i2c` (`cart_id`),
  KEY `FKhoclkgyvt1jydh7wjrcbrinro` (`inventory_item_id`),
  KEY `FK1re40cjegsfvw58xrkdp6bac6` (`product_id`),
  CONSTRAINT `FK1re40cjegsfvw58xrkdp6bac6` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FKhoclkgyvt1jydh7wjrcbrinro` FOREIGN KEY (`inventory_item_id`) REFERENCES `inventory_items` (`id`),
  CONSTRAINT `FKpcttvuq4mxppo8sxggjtn5i2c` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart_items`
--

LOCK TABLES `cart_items` WRITE;
/*!40000 ALTER TABLE `cart_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `carts` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `created_on` date NOT NULL,
  `total` double DEFAULT NULL,
  `total_items` int DEFAULT NULL,
  `updated_on` date NOT NULL,
  `user_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKb5o626f86h46m4s7ms6ginnop` (`user_id`),
  CONSTRAINT `FKb5o626f86h46m4s7ms6ginnop` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carts`
--

LOCK TABLES `carts` WRITE;
/*!40000 ALTER TABLE `carts` DISABLE KEYS */;
INSERT INTO `carts` VALUES (1,'2023-03-10',0,0,'2023-03-10',6),(2,'2023-03-10',0,0,'2023-03-10',7),(3,'2023-03-10',0,0,'2023-03-13',8),(4,'2023-04-01',0,0,'2023-04-01',9);
/*!40000 ALTER TABLE `carts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventories`
--

DROP TABLE IF EXISTS `inventories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inventories` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `total_price` double DEFAULT NULL,
  `total_quantity` int DEFAULT NULL,
  `user_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKi2stcrw4fk7tn0pr06qvtgc1x` (`user_id`),
  CONSTRAINT `FKi2stcrw4fk7tn0pr06qvtgc1x` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventories`
--

LOCK TABLES `inventories` WRITE;
/*!40000 ALTER TABLE `inventories` DISABLE KEYS */;
INSERT INTO `inventories` VALUES (1,717750,11000,1),(2,20443120,327000,2),(3,1810000,6000,3),(4,4997200,51000,4),(5,0,0,5),(6,0,0,6),(7,0,0,7),(8,6514600,85000,8),(9,0,0,9),(10,0,0,10),(11,0,0,11);
/*!40000 ALTER TABLE `inventories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_items`
--

DROP TABLE IF EXISTS `inventory_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inventory_items` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `prod_exp_date` date NOT NULL,
  `prod_mfg_date` date NOT NULL,
  `price` double NOT NULL,
  `quantity` int NOT NULL,
  `inventory_id` bigint NOT NULL,
  `product_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmckma5ra1ec4pu4of4ck8wpuq` (`inventory_id`),
  KEY `FK9qhblf3mc4r22jajlv4w6sstt` (`product_id`),
  CONSTRAINT `FK9qhblf3mc4r22jajlv4w6sstt` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FKmckma5ra1ec4pu4of4ck8wpuq` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_items`
--

LOCK TABLES `inventory_items` WRITE;
/*!40000 ALTER TABLE `inventory_items` DISABLE KEYS */;
INSERT INTO `inventory_items` VALUES (3,'2024-12-31','2022-01-01',65250,1000,1,9),(4,'2024-02-01','2022-08-01',652500,10000,1,9),(8,'2025-02-01','2023-01-01',1500000,15000,2,10),(11,'2025-02-01','2023-01-01',1500000,15000,2,10),(12,'2025-02-01','2023-01-01',100000,1000,2,10),(13,'2025-02-01','2023-01-01',3000000,30000,2,10),(14,'2025-02-01','2023-01-01',500000,5000,2,10),(15,'2025-02-01','2023-01-01',552750,15000,2,8),(16,'2025-02-01','2023-01-01',737000,20000,2,8),(17,'2025-02-01','2023-01-01',1474000,40000,2,8),(18,'2025-02-01','2023-01-01',184250,5000,2,8),(20,'2024-02-01','2022-08-01',73360,2000,8,2),(21,'2024-10-01','2022-08-01',147400,4000,8,8),(22,'2025-12-31','2023-02-01',3600000,30000,8,1),(23,'2025-12-31','2023-02-01',3500400,30000,4,4),(24,'2026-02-28','2022-08-01',1166800,10000,4,4),(25,'2024-12-31','2022-01-01',30000,1000,4,5),(26,'2024-02-01','2022-08-01',300000,10000,4,5),(27,'2024-02-01','2022-08-01',438400,2000,3,3),(28,'2024-10-01','2022-08-01',1371600,4000,3,7),(29,'2023-12-31','2021-12-01',100000,1000,2,10),(30,'2025-02-01','2023-01-01',1500000,15000,2,10),(31,'2025-02-01','2023-01-01',586880,16000,2,2),(32,'2025-02-01','2023-01-01',733600,20000,2,2),(33,'2025-02-01','2023-01-01',1500000,15000,2,10),(34,'2025-02-01','2023-01-01',100000,1000,2,10),(35,'2025-02-01','2023-01-01',3000000,30000,2,10),(36,'2025-02-01','2023-01-01',500000,5000,2,10),(37,'2025-02-01','2023-01-01',552750,15000,2,8),(38,'2025-02-01','2023-01-01',737000,20000,2,8),(39,'2025-02-01','2023-01-01',1474000,40000,2,8),(40,'2025-02-01','2023-01-01',184250,5000,2,8),(41,'2026-02-28','2022-08-01',1200000,10000,8,1),(42,'2025-02-01','2023-01-01',586880,16000,8,2),(43,'2025-02-01','2023-01-01',733600,20000,8,2),(44,'2023-12-31','2021-12-01',100000,1000,8,10);
/*!40000 ALTER TABLE `inventory_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notifications` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `exp_date` date NOT NULL,
  `message` varchar(50) DEFAULT NULL,
  `prod_name` varchar(100) NOT NULL,
  `quantity` int NOT NULL,
  `user_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9y21adhxn0ayjhfocscqox7bh` (`user_id`),
  CONSTRAINT `FK9y21adhxn0ayjhfocscqox7bh` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_details` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `total_price` double NOT NULL,
  `quantity` int NOT NULL,
  `inventory_item_id` bigint DEFAULT NULL,
  `order_id` bigint NOT NULL,
  `product_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK33bhgkb9bh62515xkov5tb91f` (`inventory_item_id`),
  KEY `FKjyu2qbqt8gnvno9oe9j2s2ldk` (`order_id`),
  KEY `FK4q98utpd73imf4yhttm3w0eax` (`product_id`),
  CONSTRAINT `FK33bhgkb9bh62515xkov5tb91f` FOREIGN KEY (`inventory_item_id`) REFERENCES `inventory_items` (`id`),
  CONSTRAINT `FK4q98utpd73imf4yhttm3w0eax` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FKjyu2qbqt8gnvno9oe9j2s2ldk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
INSERT INTO `order_details` VALUES (1,73360,2000,20,1,2),(2,3600000,30000,22,2,1),(3,147400,4000,21,3,8),(4,1200000,10000,41,6,1),(5,586880,16000,42,7,2),(6,733600,20000,43,7,2),(7,100000,1000,44,8,10),(8,3500400,30000,23,9,4);
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `delivery_date` date NOT NULL,
  `order_date` date NOT NULL,
  `order_total` double NOT NULL,
  `pay_status` varchar(6) DEFAULT NULL,
  `order_status` varchar(10) DEFAULT NULL,
  `total_order_items` int NOT NULL,
  `ws_id` bigint DEFAULT NULL,
  `man_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKb4cxgcgbhjec263koqlj54m4h` (`ws_id`),
  KEY `FKdetdypamn6x882tbloj0yuxrl` (`man_id`),
  CONSTRAINT `FKb4cxgcgbhjec263koqlj54m4h` FOREIGN KEY (`ws_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FKdetdypamn6x882tbloj0yuxrl` FOREIGN KEY (`man_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'2023-03-10','2023-03-10',73360,'PAID','DELIVERED',1,8,2),(2,'2023-03-10','2023-03-10',3600000,'PAID','DELIVERED',1,8,1),(3,'2023-03-10','2023-03-10',147400,'PAID','DELIVERED',1,8,2),(6,'2023-03-10','2023-03-10',1200000,'PAID','DELIVERED',1,8,1),(7,'2023-03-10','2023-03-10',1320480,'PAID','DELIVERED',2,8,2),(8,'2023-03-13','2023-03-13',100000,'PAID','DELIVERED',1,8,2),(9,'2023-03-18','2023-03-13',3500400,'UNPAID','PLACED',1,8,4);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `contents` varchar(255) NOT NULL,
  `url` varchar(1000) DEFAULT NULL,
  `margin` float NOT NULL,
  `prod_name` varchar(100) NOT NULL,
  `prod_price` float NOT NULL,
  `user_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_fkd6i26ygbk4yg6thqqw7erhi` (`prod_name`),
  KEY `FKdb050tk37qryv15hd932626th` (`user_id`),
  CONSTRAINT `FKdb050tk37qryv15hd932626th` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'abcd','https://onemg.gumlet.io/c_fit,f_auto,w_150,q_auto,h_150/cropped/pydoe2cjed0vjf2or3kk.jpg',0.2,'ABPhylline 100mg Capsule',120,1),(2,'fgyt','https://onemg.gumlet.io/c_fit,f_auto,h_150,q_auto,w_150/cropped/g19y0bxvqdfaz8kcvhw9.jpg',0.2,'Monotrate 20mg Tablet 10 S',36.68,2),(3,'adef','https://onemg.gumlet.io/f_auto,w_150,c_fit,h_150,q_auto/cropped/agqmtzpws9jrcgzek60z.jpg',0.2,'Ezact MR Tablet',219.2,3),(4,'fhjgt','https://onemg.gumlet.io/c_fit,q_auto,f_auto,h_150,w_150/cropped/pqscuxcf4frfco0war3m.jpg',0.2,'Scabex Lotion',116.68,4),(5,'fhjgt','https://onemg.gumlet.io/c_fit,q_auto,f_auto,h_150,w_150/cropped/pqscuxcf4frfco0war3m.jpg',0.2,'NT EmScab Lotion',30,4),(6,'fhjgt','https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.pharmahopers.com%2Fassets%2Fimages%2Fproducts%2Fc92f5-NTSCAB-LOTION.jpg&imgrefurl=https%3A%2F%2Fcompany.pharmahopers.com%2Fronish-bioceuticals%2Fproduct%2Fntscab-lotion&tbnid=uV1AJqEEb2U2GM&vet=12ahUKEwjr1OKg-K39AhUpJLcAHeCmA24QMygCegQIARBB..i&docid=m7TJXLJQD3_uxM&w=500&h=500&itg=1&q=ntscab%20lotion&ved=2ahUKEwjr1OKg-K39AhUpJLcAHeCmA24QMygCegQIARBB',0.2,'NT Scab Lotion',28.56,4),(7,'fhjgtcs','https://onemg.gumlet.io/c_fit,f_auto,h_150,q_auto,w_150/hth9j7ydtxjqqqpfhhpn.jpg',0.2,'Naturolax-A range Flavour Powder 300 gm',342.9,3),(8,'osmic paper','https://onemg.gumlet.io/l_watermark_346,w_120,h_120/a_ignore,w_120,h_120,c_fit,q_auto,f_auto/rzlshwibctfr2enkne28.jpg',0.2,'i-can pregnancy test kit',36.85,2),(9,'fhjght','https://onemg.gumlet.io/f_auto,q_auto,h_150,w_150,c_fit/rflk06bodrte0gzbuyy2.jpg',0.2,'Lacto Calamine Oil Balance for Oily Skin Daily Face Care Lotion 60 ml',65.25,1),(10,'sfrzd','https://onemg.gumlet.io/f_auto,w_150,c_fit,h_150,q_auto/e2cb967e1e6e4d9faeb27fc28828cfdd.jpg',0.2,'Advanced Joins Muscle Spray',100,2),(11,'sfrzd','https://onemg.gumlet.io/l_watermark_346,w_120,h_120/a_ignore,w_120,h_120,c_fit,q_auto,f_auto/eibiu6luyohdy0dwscvp.jpg',0.2,'Sloans Spray',161.25,3);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `email` varchar(30) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `password` varchar(20) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `user_role` varchar(30) DEFAULT NULL,
  `city` varchar(30) NOT NULL,
  `district` varchar(30) NOT NULL,
  `gst_number` varchar(15) NOT NULL,
  `lane` varchar(30) NOT NULL,
  `org_name` varchar(100) NOT NULL,
  `pincode` int NOT NULL,
  `state` varchar(30) NOT NULL,
  `taluka` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_6dotkott2kjsp8vw4d0m25fb7` (`email`),
  UNIQUE KEY `UK_du5v5sr43g5bfnji4vb8hg5s3` (`phone`),
  UNIQUE KEY `UK_llafvrypmyedpfhha3kxbriy3` (`gst_number`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'sunpharma@gmail.com','Dilip','Shanghvi','mypassword1','2243244324','MANUFACTURER','Katraj','Pune','2TEECC56163PIZR','48A','Sun Pharma',411043,'Maharashtra','Pune City'),(2,'lupinpharma@gmail.com','Desh','Gupta','mypassword2','2266402323','MANUFACTURER','Chakan','Pune','27AAACL1069K5ZB','E-3','Lupin Pharma',410501,'Maharashtra','Khed'),(3,'glenmarkpharma@gmail.com','Gracias','Saldanha','mypassword3','2240189999','MANUFACTURER','Baramati','Pune','22AAACG2207L1Z2','E-219','Glenmark Pharma',413133,'Maharashtra','Baramati'),(4,'piramalpharma@gmail.com','Ajay4','Piramal','mypassword4','2233448999','MANUFACTURER','Hinjawadi','Pune','27AALCP0909M2Z0','Phase 3','Piramal Enterprises Limited',411057,'Maharashtra','Mulshi'),(5,'ciplapharma@gmail.com','Hamied','Khwaja','mypassword5','9822514691','MANUFACTURER','Daund','Pune','22AAACC1450B1ZO','Gandhi Chauk','Cipla Limited',413801,'Maharashtra','Daund'),(6,'kp@gmail.com','Kedar','Urunkar','mypassword6','9325507941','WHOLESALER','Ichalkaranji','Kolhapur','22AAACC1450B1ZZ','Gandhi Chauk','KD bandda Limited',416115,'Maharashtra','Hatkanangle'),(7,'sp@gmail.com','Sushant','Patil','mypassword8','7083820956','WHOLESALER','Saroli','Kolhapur','22AAACC1450B007','Kasarwada Road','One Shot Solutions',416504,'Maharashtra','Gadhinglaj'),(8,'st@gmail.com','Stalin','Goves','mypassword9','7758835519','WHOLESALER','Harli Kh','Kolhapur','22AAACC1450B008','Near Nadives','One Shot Life',416502,'Maharashtra','Gadhinglaj'),(9,'ap@gmail.com','Amit','Patil','amit','9933445522','WHOLESALER','Saroli','Kolhapur','DPKET92HCN59SW1','Kasarwada Road','Kubota',416504,'Maharashtra','Gadhinglaj'),(10,'exmp@gmail.com','sdv','sf ','amit','4567891236','MANUFACTURER','UB','KJ ','MDP4YA9VCJ19DLS','HVK','ljdv',789456,'KJ','KH '),(11,'ak@gmail.com','','','amit','','MANUFACTURER','','','','','',0,'','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-04-02 16:01:16
